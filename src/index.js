import restify from "restify";
import restifyPlugins from "restify-plugins";
import config from "./config/config";
import getUsers from "./controllers/users";

const server = restify.createServer({
  name: config.name,
  version: config.version
});

server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());

server.get("/users", getUsers);

server.listen(config.port, () => {
  console.log(`${server.name} listening at ${config.base_url}`);
});
