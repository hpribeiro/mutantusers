import request from "async-request";
import { sortBy, pick } from "../utils/utils";

const GET_USERS_URL = "https://jsonplaceholder.typicode.com/users";

async function requestUsers() {
  return request(GET_USERS_URL);
}

function selectUserFields(user) {
  return pick(user, ["name", "email", "website", "address", "company"]);
}

function orderUsersBy(users, key) {
  return sortBy(users, key);
}

function validateResponse(response) {
  if (response && response.statusCode !== 200) {
    throw new Error("Error to get users");
  }
}

function filterUsersWithSuite(users) {
  return users.filter(user =>
    user.address.suite.toLowerCase().includes("suite")
  );
}

function parseUsers(users) {
  let responseUsers = [...users];
  responseUsers = filterUsersWithSuite(responseUsers);
  responseUsers = responseUsers.map(user => selectUserFields(user));
  responseUsers = orderUsersBy(responseUsers, "name");
  return responseUsers;
}

function validateUsers(users) {
  if (!users || users.length === undefined) {
    throw new Error("Users not valid");
  }
}

export default async function getUsers(req, res, next) {
  try {
    let response;
    try {
      response = await requestUsers();
    } catch (error) {
      throw new Error("Failed to request users");
    }

    validateResponse(response);
    let users = JSON.parse(response.body);
    validateUsers(users);
    users = parseUsers(users);
    res.send(users);
  } catch (error) {
    res.send(500, error.message);
  }
  return next();
}
