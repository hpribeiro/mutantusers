export function pick(object, keys) {
  return keys.reduce((obj, key) => {
    const accObj = { ...obj };
    if (object && !!object[key]) {
      accObj[key] = object[key];
    }
    return accObj;
  }, {});
}

export const sortBy = (arr, key) => {
  return arr.concat().sort((a, b) => {
    let comparator = 0;
    if (a[key] > b[key]) {
      comparator = 1;
    } else if (b[key] > a[key]) {
      comparator = -1;
    }
    return comparator;
  });
};
