# mutantusers

An awesome module

## Install

npm:

    npm i mutantusers

## Usage

Do a HTTP GET to <http://localhost:8080/users>
to retrieve the users list

npm start

## API

<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

## License

MIT © [Henrique Ribeiro](https://github.com/hpribeiro)
