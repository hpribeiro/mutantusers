import sayHello from "../src";

test("sayHello", () => {
  expect(sayHello()).toBe("Hello, Henrique!");
  expect(sayHello("foo")).toBe("Hello, foo!");
});
